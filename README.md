## The OP: (It's dangerous to go alone! Take this:)
More info at [the bottom](#need-to-update-or-make-changes-to-the-op-or-anything).

```
>"#GamerGate?" Read:
• FAQ: http://v.gd/mnxDuF
• TL;DR: http://i.imgur.com/StBV0VX.png

>Summarys of "#GamerGate":
https://archive.today/23Fde - GamerGate: A State of the Union Address
https://www.youtube.com/watch?v=ipcWm4B3EU4 - #GamerGate in 60 Seconds

>Reminders (Important, READ THESE!):
• Use https://archive.today to deny sites ad revenue and traffic, and to archive tweets against later deletion. (donotlink.com is useless for any of these purposes. It only blocks Google PageRank link juice. Do not use it!)
• Be civil if you have to argue with people on Twitter. Don't make us look like douchebags.
• Ignore derailers and shills: http://v.gd/RJESEU
• Do not accept requests for '''any''' goal or demand list: http://pastebin.com/p5dVp1e5
• It is trivially easy to change one's 8chan user ID. Do not rely on it for identifying 8chan user persistence. A non-shill's 8chan user ID may be spoofed by a shill, or an identified shill may change their 8chan user ID to throw off the heat.

>How can I help?:
• E-mail advertisers: http://v.gd/C29HQs
• Find connections and corruption: http://v.gd/EXFUT9
• Post on Twitter: http://v.gd/TjIYWG
• Educate yourself on logical debating: http://v.gd/kggp5z

>Current happenings (Help and make pull requests!):
http://v.gd/JpffwX

>Articles and blog posts (Please spread these!):
• General articles: http://v.gd/JUIShm
• Articles re: corruption in games jounalism: http://v.gd/o6CY9w
• APGNation interviews William Usher, the man behind the GameJournoPros leaks:
http://apgnation.com/archives/2014/09/29/7694/breaking-the-chain-an-interview-with-william-usher
• New advertiser email info from everyone's favorite insider source:
http://blogjob.com/oneangrygamer/2014/09/gamergate-game-journo-pro-member-explains-how-pull-advertiser-support/
• TechCrunch featuring a fair & informative article from an anon:
http://techcrunch.com/2014/09/25/gamergate-an-issue-with-2-sides/
• Complete dismantling of latest shill tactic of claiming #GamerGate started with harrassment:
https://medium.com/@cainejw/a-narrative-of-gamergate-and-examination-of-claims-of-collusion-with-4chan-5cf6c1a52a60

>Boycotted brands:
http://v.gd/YUsbp7

>Supported brands:
http://a.pomf.se/qcagft.jpg
http://alternatives.gamergate.community/

>Boycott list, Adblock Plus list adaptation, support list, graph list:
http://git.io/nQ_4yg

>Other discussion locations:
• The Escapist: http://www.escapistmagazine.com/forums/read/18.860762-GamerGate-Discussion-Debate-and-Resources
• Reddit: http://www.reddit.com/r/KotakuInAction/
• #GamerGate Community: http://gamergate.community/

>Regular GamerGate vloggers & streamers:
• Video news & content schedule: http://v.gd/lSj5Fi (Help and make pull requests!)
• KingofPol: http://www.hitbox.tv/KingofPol
• Whiskey Grenade: http://www.hitbox.tv/whiskeygrenade
• KotakuInAction/Hatman: https://www.youtube.com/channel/UCWcUJMh7z--7g71k9rYzd1A
• Shield Project/#NotYourShield: https://www.youtube.com/channel/UC8qlxxl1MJG4kbwTAtGINnA

>IRC:
• #GamerGate discussion: #burgersandfries @ rizon
• Off-topic discussion: #friesandburgers @ rizon
• Gitorious/Gitgud discussion: #4free @ rizon

>Get all your copypasta here:
http://v.gd/UiBGLE

>Full OP text (Want to change anything? Make an issue or pull request!):
http://gitgud.net/gamergate/gamergateop/blob/master/README.md

>YouTube playlist:
https://www.youtube.com/playlist?list=PLjN8qonwEtowetQWBPh0IUhPFBwZnmDOh
```

### <a name="need-to-update-or-make-changes-to-the-OP-or-anything"></a>Need to update or make changes to the OP (or anything)?
1. Make an account on [Gitgud](http://gitgud.net/) ([Click here to Sign Up](http://gitgud.net/users/sign_up), make a [cock.li](https://cock.li/) email if you want to keep your normal email safe from doxing).
2. Submit an [Issue](http://gitgud.net/gamergate/gamergateop/issues/new?issue[assignee_id]=&issue[milestone_id]=) OR [Fork the Repository](https://gitgud.net/gamergate/gamergateop/clone), make your changes, then submit a Merge Request  
   OR post your changes/updates/issues to the IRC #4free @ rizon.

### Things to note when editing the OP:
#### 8chan:
OP posts have a 5000 character limit. <a href="#text_stat_tool">Please make sure that the OP does not exceed this limit.</a>

OP posts have a 40 link limit [as of 2014-09-30](https://twitter.com/infinitechan/status/516911792103362560) (was 20 before). [Please make sure that the OP does not exceed this limit.](#text-stat-tool)

#### 4chan:
OP posts have a 2000 character limit and no link limit (URL shorteners trigger the spam filter, with the exception of git.io links). [Please make sure that the OP does not exceed this limit.](#text-stat-tool)

However, due to logistical difficulties on 4chan (many included terms causing auto-saging, mods quickly deleting #GamerGate OPs, and the increasing amount of content being impossible to fit within 2000 characters), assume that the OP will no longer be posted on 4chan. Therefore, the 2000 character limit is no longer in effect.

#### <a name="text-stat-tool"></a>Text stat tool:

Use http://textmechanic.com/Count-Text.html to count both characters and links. Paste text with **Instant** checked to get a character count. Then check **Custom count** and type "http" to get a link count.

# Want to help?
Because #GamerGate is so fast moving, a lot of work needs to be done to keep the repository up to date.  
If any new important events occur, and they aren't covered in the repository [Submit an Issue] about it.  
Additionally, check out the [Issues](http://gitgud.net/gamergate/gamergateop/issues) for things that you can do to help.

You can also post any updates/changes/issues in the IRC, #4free @ rizon.

If you want to save tweets for evidence check out [Tweet Save](http://tweetsave.com/)

### Want to discuss the OP or anything else GitGud related?
Hop onto the IRC #4free @ rizon.

### Contributing?
Check out [the contributing guide](http://gitgud.net/gamergate/gamergateop/tree/master/How-to-Contribute)!  If you have feedback to make it better, [please comment in its Issue](http://gitgud.net/gamergate/gamergateop/issues/6)

[Submit an Issue]:http://gitgud.net/gamergate/gamergateop/issues/new?issue[assignee_id]=&issue[milestone_id]=