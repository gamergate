# Operation Disrespectful Nod

<div id="contents" style="visibility: hidden;"></div>
### Contents:

1. [Conduct Guide](#conduct-guide)
2. [Articles to Email](#email-the-articles)
3. [Who to Email](#to-their-respectives-site-39-s-advertisers-be-polite)
  1. [Polygon (exclusively)](#polygon-exclusively)
  2. [Gawker & Polygon](#gawker-amp-kotaku)
  3. [Gawker & Kotaku](#gawker-amp-kotaku)
  4. [Gawker (exclusively)](#gawker-exclusively)
  5. [Kotaku (exclusively)](#kotaku-exclusively)
  6. [Rock Paper Shotgun](#rock-paper-shotgun)
  7. [Gamasutra](#gamasutra)
4. [References](#references)
5. [Advertisers who have withdrawn](http://gitgud.net/gamergate/gamergateop/blob/master/Operation%20Disrespectful%20Nod/advertisers-who-have-withdrawn.md)
6. [Helpful Guides](#helpful-guides)


<div id="conduct-guide" style="visibility: hidden;"></div>
### [\[⇧\]](#contents) CONDUCT GUIDE

Just came here and not sure what to do?
- Read the following: https://archive.today/OFWFS then...
- Come back here for the full list of places to email

<div id="email-the-articles" style="visibility: hidden;"></div>
### [\[⇧\]](#contents) EMAIL THE ARTICLES

#### "Gamers are Dead" articles:

*'Gamers' don't have to be your audience. 'Gamers' are over.* Exclusive - Leigh Alexander of **Gamasutra** *(28th Aug)*  
https://archive.today/l1kTW

*A Guide to Ending "Gamers"* - Devin Wilson of **Gamasutra** *(28th Aug)*  
https://archive.today/2t93l

*We Might Be Witnessing The 'Death of An Identity'* - Luke Plunkett of **Kotaku** *(28th Aug)*  
https://archive.today/YlBhH

*An awful week to care about video games* - Chris Plante, **Polygon** *(28th Aug)*  
https://archive.today/rkvO8

*The Monday Papers* - Graham Smith, **Rock Paper Shotgun** *(1st Sep)*  
https://archive.today/RXjWz

*The End of Gamers* - Dan Golding, [Director of Freeplay Independent Games Festival](https://archive.today/UMYkA) *(28th Aug)*  
https://archive.today/L4vJG

*The death of the “gamers” and the women who “killed” them* - Casey Johnson of **Ars Technica** *(28th Aug)*  
https://archive.today/i928J

*It's Dangerous to Go Alone: Why Are Gamers So Angry?* - Arthur Chu, **The Daily Beast** *(28th Aug)*  
https://archive.today/9NxHy

*Gaming Is Leaving “Gamers” Behind* - Joseph Bernstein, **Buzzfeed** *(28th Aug)*  
https://archive.today/jVqJ8

*Sexism, Misogyny, and online attacks: It's a horrible time to consider yourself a gamer* - Patrick O'Rourke, **Financial Post** *(28th Aug)*  
https://archive.today/HkPHc

#### Other articles released on the 28th Aug:

*Misogynistic trolls drive feminist video game critic from her home* - Callie Beusman, **Jezebel**  
https://archive.today/kXX7y

*A disheartening account of the harassment going on in gaming right now (and how Adam Baldwin is involved)* - Victoria McNally, **The Mary Sue**  
https://archive.today/11OEl

*Feminist video bloggers driven from home by death threats* - Jack Smith IV, **BetaBeat**  
https://archive.today/zHH4T

*Anita Sarkeesian threatened with rape and murder for daring to keep critiquing video games* - Anna Minard, **The Stranger**  
https://archive.today/StChn

*Fanboys, white knights, and the hairball of online misogyny* - Tauriq Moosa, **The Daily Beast**  
https://archive.today/wptL5

<div id="to-their-respectives-site-39-s-advertisers-be-polite" style="visibility: hidden;"></div>
### [\[⇧\]](#contents) TO THEIR RESPECTIVES SITE'S ADVERTISERS (BE POLITE)

<div id="polygon" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) POLYGON (exclusively):

###### KRAFT

http://ir.kraftfoodsgroup.com/contactus.cfm - EMAIL FORM

###### SCOTTRADE (Pulled their ads, [Source](http://https://twitter.com/tacosforjustice/status/509487357817798659/photo/1))

~~https://www.scottrade.com/contact/contact-form.html~~

###### eHEALTH

https://www.ehealthinsurance.com/ehi/help/emailcontact?productType=IFP&contact=IFP

###### SMITH'S, A DIVISION OF KROGER

https://www.kroger.com/topic/contact-us


------------
<div id="gawker-amp-polygon" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) GAWKER & POLYGON:

###### VERIZON [⁽³⁾]

robert.a.varettoni@verizon.com < EXECUTIVE DIRECTOR OF MEDIA RELATIONS, SO BE FUCKING POLITE

raymond.mcconville@verizon.com < MANAGER OF MEDIA RELATIONS, SO STILL BE FUCKING POLITE

edward.s.mcfadden@verizon.com < EXECUTIVE DIRECTOR OF PUBLIC POLICY, YOU KNOW THE DRILL


------------
<div id="gawker-amp-kotaku" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) GAWKER & KOTAKU:

###### Intel [⁽³⁾]
[Submit a form here](https://www-ssl.intel.com/content/www/us/en/forms/corporate-responsibility-contact-us.html)

###### Motorola [⁽³⁾]
[General inquiry submission form](https://motorola-global-portal.custhelp.com/app/chat/chat_launch/p_country_code/US/p_reference/moto_integration.php)  
[Contact form](https://forums.motorola.com/contact)  
Twitters: [@Motorola](https://twitter.com/Motorola), [@MotoChannelNA](https://twitter.com/MotoChannelNA), [@MotoSolutions](https://twitter.com/MotoSolutions)  
bill.abelson@motorola.com - Bill Abelson, [Public Relations Contact for Motorola](https://archive.today/LY9OQ), be polite!  
Additional contacts: http://www.symbol.com/news/reporters_only/fast_facts.html

###### State Farm [⁽³⁾]
[Media contacts page](https://www.statefarm.com/about-us/newsroom/media-contacts)  
Phone number: 309-766-2311 - Customer Service, Marketing and Advertising, Sales, and General Inquiries  
CEO Email: Ed.Rust.atei@statefarm.com  
PR Emaill: holly.anderson.m3mj@statefarm.com  

###### SAMSUNG [⁽¹⁾] [⁽³⁾]

d2.cohen@sea.samsung.com - Danielle Meister Cohen, US CORPORATE MEDIA RELATIONS  
samsungpr@edelman.com - Edelman, PR TEAM  
o.kwon@samsung.com - CEO, BE POLITE!  

###### SPRINT [⁽¹⁾] [⁽³⁾]

sprintcares@sprint.com  
jeff_.d.hallock@sprint.com - [Jeff Hallock, CHIEF MARKETING OFFICER](https://archive.today/27XnO), BE POLITE!  
brad.d.hampton@sprint.com - [Brad Hampton, VICE PRESIDENT](https://archive.today/VPkiX), BE POLITE!  
[@Sprintcare](https://twitter.com/Sprintcare) - Twitter  

###### AMC [⁽¹⁾] [⁽³⁾]

amccustomerservice@amcnetworks.com  
AMCPressHelp@amcnetworks.com  
digitaladsales@amctv.com - [Chris Cifarelli, DIRECTOR, DIGITAL AD SALES](http://web.archive.org/web/20141010195948/http://www.amctv.com/contact), BE POLITE!  
[@jrc1251 (twitter)](https://twitter.com/jrc1251) - [Jessica Cox, DIRECTOR, DIGITAL AND DIRECT MARKETING](https://archive.today/juozj), BE POLITE!  

###### ELECTRONIC ARTS (LOL) [⁽¹⁾] [⁽³⁾]

CUSTOMERSERVICE@EA.COM

###### SCION (Toyota Company) [⁽¹⁾] [⁽³⁾]

http://scion.custhelp.com/app/ask  
Jmoreno@tma.toyota.com - Javier Moreno, TOYOTA MEDIA CONTACT  

###### OLD SPICE (Procter & Gamble Company) [⁽¹⁾] [⁽³⁾]

dicarlo.km@pg.com - Kate DiCarlo, PROCTER & GAMBLE MEDIA CONTACT

jessica.johnston@citizenrelations.com - Jessica Johnston, CITIZEN RELATIONS MEDIA CONTACT

------------

<div id="gawker-exclusively" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) GAWKER (exclusively):

###### MICROSOFT & BING [⁽³⁾]
According to [Microsoft's website](https://archive.today/wSMzh), [this agency](https://archive.today/A3qB3) is in charge of their advertising PR.  
prny@mccann.com - the e-mail listed on their website  
Bing twitter: [@Bing](https://twitter.com/Bing)

###### IBM [⁽³⁾]
Contact form: http://www.ibm.com/scripts/contact/contact/us/en

###### American Express [⁽³⁾]
Phone number: 1-800-954-0559  
[@AskAmex](https://twitter.com/AskAmex) - Twitter  

###### HP [⁽³⁾]
General non-support Contact Form: https://ssl.www8.hp.com/us/en/company-information/executive-team/ceo-index.html

###### Google [⁽³⁾]
http://www.wikihow.com/Contact-Google - phone numbers for customer service based on country.  
press@google.com  

###### Blackberry [⁽³⁾]
mediarelations@blackberry.com  
[@BlackBerry](https://twitter.com/blackberry) - Twitter  

###### Virgin Mobile [⁽³⁾]
A contact form can be found [here](https://www2.virginmobileusa.com/about/prepareEmailUs.do), but it's only for people who use Virgin Mobile already.  
The page does include a phone number for general use, however.  

###### Acer [⁽³⁾]
It seems direct customer service is on [this page](http://us.acer.com/ac/en/US/content/service-contact), but only avaiable to Acer customers.  
408-533-7700 - General phone number  
[@Acer](https://twitter.com/Acer) - Twitter  

###### Dove Men Care [⁽³⁾]
Contact form: https://secure.dove.us/About-Dove/Contact-Us/contactusform.aspx  
Contact their parent company, Unilever: http://www.unilever.com/resource/Contactus/index.aspx  
Dove is currently supporting "campaigns for self-esteem," and would probably be very concerned to hear about Gawker's advocacy of bullying.  

###### Glaceau SmartWater [⁽³⁾]
Reached through the main Coca-Cola Company page: https://secure.coca-colacompany.com/ssldocs/mail/eQuery_product.shtml

###### Comcast [⁽³⁾]
We_Can_Help@cable.comcast.com

###### HBO [⁽³⁾]
http://www.hbo.com/index.html#/about/contact-us.html  
Quentin.Schaffer@hbo.com - Quentin Schaffer, [SR. VICE PRESIDENT COPORATE COMMUNICATIONS](https://archive.today/ueuHO), BE POLITE!  
Jeff.Cusson@hbo.com - Jeff Cusson, [SR. VICE PRESIDENT CORPORATE AFFAIRS](https://archive.today/ZnReZ), BE POLITE!  

###### LogMeIn [⁽³⁾]
Contact form: http://help.logmein.com/SelfServiceContact  
[@LogMeIn](https://twitter.com/LogMeIn) - Twitter  

###### Johnnie Walker [⁽³⁾]
keepwalking@johnniewalker.com

###### Radio Shack [⁽³⁾]
Contact form: http://www.radioshack.com/helpdesk/index.jsp?stillHaveQuestion=yes&subdisplay=contact&display=store

###### BMW [⁽³⁾]
customerrelations@bmwusa.com

###### EBAY [⁽³⁾]
gdebernardo@ebay.com - Guillermo Debernardo, [Head of Marketing for eBay Inc.](https://archive.today/6dvYL) BE POLITE!

###### HTC [⁽³⁾]
Contact form: http://www.htc.com/us/contact/email/

###### Cisco [⁽³⁾]
http://www.cisco.com/cisco/web/siteassets/contacts/index.html  
faladin@cisco.com - Aladin, [DIRECTOR OF TECHNICAL MARKETING FOR CISCO'S ENTERPRISE NETWORKING PORTFOLIO](https://archive.today/3wJOF0), BE POLITE!  

###### T-Mobile [⁽³⁾]
No direct email, but a [support page here](http://www.t-mobile.com/contact-us.html) with options for reaching them by phone or live chat.

###### AT&T [⁽³⁾]
[More phone numbers, support forms, live chat, etc.](http://www.att.com/contactus/). I guess phone companies just don't like providing e-mail addresses, sadly.

###### Nissan [⁽³⁾]
Contact form: http://www.nissanusa.com/apps/contactus

###### Budweiser [⁽³⁾]
Contact form: http://contactus.anheuser-busch.com/contactus/ab/contact_us.asp  
media@anheuser-busch.com  
marianne.amssoms@ab-inbev.com - Marianne Amssoms, [VICE PRESIDENT GLOBAL COMMUNICATIONS, ANHEUSER-BUSCH INBEV](https://archive.today/bHq5D), BE POLITE!  
karen.couck@ab-inbev.com - Karen Couck, [GLOBAL DIRECTOR EXTERNAL COMMUNICATIONS, ANHEUSER-BUSCH INBEV](https://archive.today/LLnBV), BE POLITE!  

###### Ford [⁽³⁾]
Contact form for advertising-related topics: https://secure.corporate.ford.com/footer/contact-ford/contact-us-email?contactMainTopic=SalesAdvertising  
Contact form for public affairs: https://secure.corporate.ford.com/footer/contact-ford/contact-us-email?contactMainTopic=PublicAffairs  

###### Adobe [⁽³⁾]
They have a [general customer service page](http://helpx.adobe.com/contact.html).  
[@AdobeCare](https://twitter.com/AdobeCare) - Twitter  

###### Lexus [⁽³⁾]
Contact form: http://lexus2.custhelp.com/app/ask

###### Marriott [⁽³⁾]
Contact form: https://www.marriott.com/marriott/contact.mi

###### New Balance [⁽³⁾]
Contact form: http://www.newbalance.com/on/demandware.store/Sites-newbalance_us2-Site/default/CustomerService-ContactUs  
newbalance@pgrmedia.com  
media.relations@newbalance.com  

###### Speed Stick [⁽³⁾]
Best to go through Colgate themselves, via [this form email](http://www.colgate.com/app/Colgate/US/Corp/ContactUs/ConsumerAffairs/ContactForm.cvsp).  
[@SpeedStick](https://twitter.com/SpeedStick) - Twitter  

###### Volkswagen [⁽³⁾]
Contact form: http://www.vw.com/contact/ you'll just have to click on it to bring it up.

###### Comedy Central [⁽³⁾]
steve.albani@cc.com - Steve Albani, Senior Vice President, Communications  
jenni.runyan@cc.com - Jenni Runyan, Vice President, Communications  
renata.luczak@cc.com - Renata Luczak, Vice President, Communications  
michelle.rosenblatt@cc.com - Michelle Rosenblatt, Director, Consumer Publicity  
allie.lee@cc.com - Allie Lee, Manager, Consumer Publicity  
eve.kenny@cc.com - Eve Kenny, Publicist, Consumer Publicity  
More email contacts: http://press.cc.com/contacts  

###### Warby Parker Eyewear [⁽³⁾]
Contact form: https://www.warbyparker.com/help  
international@warbyparker.com  

###### Qualcomm Snapdragon [⁽³⁾]
corpcomm@qualcomm.com  
publicaffairs@qualcomm.com  
ir@qualcomm.com.  

###### Mercedes-Benz [⁽³⁾] ([THEY HAVE PULLED OUT!](http://gitgud.net/gamergate/gamergateop/blob/master/Operation%20Disrespectful%20Nod/advertisers-who-have-withdrawn.md))
~~Contact form: http://mercedes-benz.custhelp.com/app/ask/session/L3RpbWUvMTQxMzU0MDU1NS9zaWQvbEdOUEg0NW0%3D~~

###### Jaguar [⁽³⁾]
Contact form: https://jaguar.iservicecrm.com/f/9  
ajackso1@jaguarlandrover.com - Anna Jackson, Jaguar UK PR, be polite!  
jgriff76@jaguarlandrover.com - Jonathan Griffiths, Manager Corporate and Internation Affairs, be polite!  
lpalmer1@jaguarlandrover.com - Lisa Palmer, Senior Communications Officer, be polite!  
More contacts at the bottom of most articles at http://newsroom.jaguarlandrover.com/en-in/jlr-corp/news

###### The CW [⁽³⁾]
Contact form: http://www.cwtv.com/feedback/comments/  
paul.hewitt@cwtv.com - Paul Hewitt, [SENIOR VICE PRESIDENT, NETWORK COMMUNICATIONS](https://archive.today/nhL5E), BE POLITE!  

###### Hulu [⁽³⁾]
Contact form: http://www.hulu.com/help#email

###### Smart USA [⁽³⁾]
Contact form: http://www.smartusa.com/contact/ click on "General Requests and Comments"

###### BBC America [⁽³⁾]
Contact form: http://www.bbcamerica.com/contact-us/

###### Sundance Channel [⁽³⁾]
Part of AMC Networks, so you can reuse the AMC contact list, or contact:  
amccustomerservice@amcnetworks.com  
Institute@sundance.org  

###### Focus Features [⁽³⁾]
focusfeaturespress@thaweb.com

###### Yahoo [⁽³⁾]
Some limited options [here](https://help.yahoo.com/kb/contacting-yahoo-customer-care-support-sln5856.html?impressions=true), but like a lot of the bigger companies they are rather lacking.

###### Bonobos [⁽³⁾]
You can find their [Contact a Ninja page here](http://www.bonobos.com/help), which seems to be their main outlet for customer service emails.

###### ABC [⁽³⁾]
Contact form: http://site.abc.go.com/site/contactus.html they have a rather strict character limit.

###### Chevrolet: [⁽³⁾]
Contact form: https://www.chevrolet.com/contact/email-us.html

------------
<div id="kotaku-exclusively" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) KOTAKU (exclusively):

###### PASTEBIN VERSION

http://pastebin.com/AAeBGdvA

###### STACK SOCIAL ([May have pulled already](http://gitgud.net/gamergate/gamergateop/blob/master/Operation%20Disrespectful%20Nod/advertisers-who-have-withdrawn.md), please [open an Issue](http://gitgud.net/gamergate/gamergateop/issues/new?issue[assignee_id]=&issue[milestone_id]=) and provide more evidence!)

support@stacksocial.com

###### IKOID

pr@ikoid.com

###### INDIE GAME

hello@dailyindiegame.com

###### NEWEGG

Wecare@newegg.com < CUSTOMER RELATIONS EMAIL.

###### MONOPRICE

SUPPORT@MONOPRICE.COM

###### AMAZON

JEFF@AMAZON.COM - FUCKING CEO, BE POLITE

swheeler@amazon.com - DIRECTOR OF ADVERTISING AND PARTNER SHIPS

selipsky@amazon.com - VICE PRESIDENT

###### BEST BUY ([They are asking for evidence that they advertise at Kotaku](https://archive.today/pex33))

laura.bishop@bestbuy.com - VICE PRESIDENT OF PUBLIC AFFAIRS

onlinestore@bestbuy.com

###### GAMESTOP

PublicRelations@GameStop.com

###### KROGER

keith.dailey@kroger.com - DIRECTOR OF MEDIA RELATIONS

###### Blizzard [⁽¹⁾]

pr@blizzard.com

rhilburger@blizzard.com - (Rob Hilburger VP of global communications, be polite!)

elrodriguez@blizzard.com - (Emil Rodriguez global public relations, be polite!)

###### ROCCAT

info@roccat.org

###### NINTENDO [⁽¹⁾]

nintendo@noa.nintendo.com

###### 2K GAMES [⁽¹⁾]

pr@2kgames.com

scott.pytlik@2k.com - Scott Pytlik, PR MANAGER  
Also on twitter [@ScottPytlik](https://twitter.com/ScottPytlik)

###### TURTLE BEACH [⁽¹⁾]

sales@turtlebeach.com

###### SONY [⁽¹⁾]

marketinginquiries@sonyusa.com

Aram_Jabbari@playstation.sony.com - Aram Jabbari, PR MANAGER AT SONY COMPUTER ENTERTAINMENT AMERICA LLC  
Also on twitter [@aramjabbari](https://twitter.com/aramjabbari)

###### TURBINE [⁽¹⁾]

pr@turbine.com

###### UBISOFT [⁽¹⁾]

support@ubisoft.com

michael.beadle@ubisoft.com - Michael Beadle, PR ASSOCIATE DIRECTOR  
scott.fry@ubisoft.com - Scott Fry, PR MANAGER  
Also on twitter [@sfry2k](https://twitter.com/sfry2k)


###### NCSOFT [⁽¹⁾]

csr@ncsoft.com ("Corporate Social Responsibilities") OR pr@ncsoft.com OR europeanpr@ncsoft.com

###### STATE FARM [⁽¹⁾]

holly.anderson.m3mj@statefarm.com - Holly Anderson, STATE FARM H.Q. MEDIA CONTACT

###### STARBUCKS [⁽¹⁾]

info@starbucks.com

###### CAPCOM [⁽¹⁾]

ir@capcom.co.jp (public relations office)

###### ENMASSE (MMORPG TERA) [⁽¹⁾]

brian@oneprstudio.com, Brian Alexander

jeane@oneprstudio.com, Jeane Wong

matwood@enmasse.com, Matt Atwood

aria@enmasse.com, Aria Brickner-McDonald

###### TRION WORLDS (ArcheAge, Rift, Defiance, Trove) [⁽¹⁾]

communications@trionworlds.com - (public relations office)

###### LIVESCRIBE [⁽¹⁾]

AmericasSales@livescribe.com OR International@livescribe.com OR CS@livescribe.com OR CS-uk@livescribe.com (UK) OR CS-au@livescribe.com (AU) ohen@sea.samsung.com

###### RAZER [⁽¹⁾]

cult@razerzone.com  
kevin.scarpati@razerzone.com - Kevin Scarpati, RAZER PR  
Also on twitter [@SDSportsGuy22](https://twitter.com/SDSportsGuy22)

###### Logitech [⁽²⁾]

mediarelations@logitech.com

Email your country's branch: http://gitgud.net/gamergate/gamergateop/tree/master/Operation%20Disrespectful%20Nod/contact%20logitech.md

---------
<div id="rock-paper-shotgun" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) ROCK PAPER SHOTGUN:

###### NVIDIA

mlim@nvidia.com - Micheal Lim, INDUSTRY ANALYST RELATIONS

rsherbin@nvidia.com - Bob Sherbin, CORP. COMMUNICATIONS LEAD

bdelrizzo@nvidia.com - Byran Del. Rizzo, GEFORCE & CONSUMER DESKTOP PRODUCTS

###### KLEI ENTERTAINMENT (Devs of Don't Starve, Eets, Shank, Mark of the Ninja)

press@kleientertainment.com - PRESS

[@coreyrollins](https://twitter.com/CoreyRollins) - Corey Rollins, MARKETING/COMMUNITY MANAGER (twitter)

[@biiigfoot](https://twitter.com/biiigfoot) [@klei](https://twitter.com/klei) - Jamie Cheng, OWNER (twitter)

---------
<div id="gamasutra" style="visibility: hidden;"></div>
#### [\[⇧\]](#contents) GAMASUTRA:

###### UAT (University of Advance Tech) [They have now pulled their ads](http://gitgud.net/gamergate/gamergateop/blob/master/Operation%20Disrespectful%20Nod/advertisers-who-have-withdrawn.md)

~~bfabiano@uat.edu - Brian Fabiano, MARKETING STRATEGIC CREATIVE DIRECTOR~~

~~ahromas@uat.edu - Alan Hromas, MARKETING MANAGER~~

~~berickson@uat.edu - Bee Erickson, MARKETING EXECUTION COORIDINATOR~~

~~atreguboff@uat.edu - Aaron Treguboff, ONLINE TRAFFIC ANALYST~~

###### MAYA/AUTODESK

noah.cole@autodesk.com - [Noah Cole, CORPORATE REPRESENTATIVE](https://archive.today/39ctg), BE POLITE!

jwright@autodesk.com - [Jeff Wright, VICE PRESIDENT, STRATEGY AND MARKETING](https://archive.today/JTqn6), BE POLITE!

dwolfe@autodesk.com - [Dawn Wolfe, SR. DIGITAL MARKETING MANAGER](https://archive.today/eJ3QV), BE POLITE!

jungahlee@autodesk.com - [Jungah Lee, WEB MARKETING MANAGER](https://archive.today/qv32U), BE POLITE!  

---------
[]: # (Superscript: ⁰ ¹ ² ³ ⁴ ⁵ ⁶ ⁷ ⁸ ⁹ ⁽ ⁾)

<div id="references" style="visibility: hidden;"></div>
# [\[⇧\]](#contents) References:
#### ⁽¹⁾ Gawker Advertising:
Live website: http://advertising.gawker.com/platform/kotaku/  
8th October 2014: https://archive.today/qFHlF  
20th August 2014: http://web.archive.org/web/20130820072327/http://advertising.gawker.com/platform/kotaku


#### ⁽²⁾ KoP Stream 8 Oct 2014:
41:10 of http://www.hitbox.tv/video/281280

#### ⁽³⁾ Gawker Partners:
1st July: http://web.archive.org/web/20140701132633/http://advertising.gawker.com/about/#partners
Screencap, 18th Oct: http://i.imgur.com/6KM54MT.png


---------
<div id="helpful-guides" style="visibility: hidden;"></div>
# [\[⇧\]](#contents) Helpful Guides
[Taco Justice](https://www.youtube.com/channel/UC2WMCIcPy1DfUz4jmkHAA4Q): #GAMERGATE NEEDS YOU TO EMAIL!  
[![Youtube link: "#GAMERGATE NEEDS YOU TO EMAIL!"](https://cdn.mediacru.sh/neOw3MAxbO8f.png)](https://www.youtube.com/watch?v=kEpSXZ6vBN0)

## Good Luck Anons!
![Screenshot](http://a.pomf.se/mfqqtv.gif)

[Part 1]:http://v.gd/N5DhWW
[Part 2]:http://v.gd/vvcnjJ
[⁽¹⁾]:#gawker-advertising
[⁽²⁾]:#kop-stream-8-oct-2014
[⁽³⁾]:#gawker-partners