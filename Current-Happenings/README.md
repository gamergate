# Current Happenings

Please submit any missing events by [forking](gitgud.net/gamergate/gamergateop/clone) and merging or [creating a new issue](gitgud.net/gamergate/gamergateop/issues/new?issue[assignee_id]=&issue[milestone_id]=).  

Other Great Timelines: [Visual Timeline](http://www.tiki-toki.com/timeline/entry/336432/The-GamerGate-Chronicles/#vars!date=2014-08-28_01:03:37!) | [KotakuInAction Timeline](https://www.reddit.com/r/KotakuInAction/wiki/index/timeline) | [historyofgamergate.com](http://www.historyofgamergate.com/) | [@cainejw early timeline analysis](https://medium.com/@cainejw/a-narrative-of-gamergate-and-examination-of-claims-of-collusion-with-4chan-5cf6c1a52a60)

Note: Dates given below are generally approximate: #Gamergate is a Worldwide Scandal.

## October 2014

### Oct 18th (Saturday)

* [William Usher leaks new evidence proving that GameJournosPro and Destructoid colluded to fire and blacklist Allistar Pinsof](http://blogjob.com/oneangrygamer/2014/10/gamergate-destructoid-corruption-and-ruined-careers/), an [illegal act in Florida](http://www.flsenate.gov/Laws/Statutes/2012/448.045) and [many other states](http://www.nolo.com/legal-encyclopedia/free-books/employee-rights-book/chapter10-9.html).


### Oct 17th (Friday)

* [Mercedes Benz has withdrawn their ads from Gawker sites!](https://pbs.twimg.com/media/B0KwVtFIEAAUyu_.jpg:large)

* [Gawker employee wants to "bring back bullying" and says "nerds should be constantly shamed and degraded into submission.".](https://pbs.twimg.com/media/B0GzE_pIQAAZt6Y.jpg:large) A number of writers and ""journalists"" [retweet and favorite this.](http://i.imgur.com/2o6lNXc.jpg)  
GamerGate responds with [an anti-bullying campaign.](https://www.crowdrise.com/gamergatestompsoutbullying/fundraiser/loping)

* [Internal email of The Guardian leaked, shows how they were biased against GamerGate from the beginning. The email contains instructions to "NOT RESPOND to this idiotic campaign" and mentions Leigh Alexander "coming in to morning conference to talk about GamerGate".](http://theralphretort.com/internal-email-shows-guardian-mind-made-gamergate/)


### Oct 16th (Thurday)

* [Jennie Bharaj, female GG-supporter, has contacted CBC news and wishes to talk about "the real narritive of GamerGate".](http://i.imgur.com/6EyhEVf.jpg)

* The New York Times wants to know: [Have You Experienced Sexism in the Gaming Industry?](https://archive.today/qPgGf)  
Also, [an article on death threats against a Literally Who was featured on the NYT's front page today, again pushing the "GamerGate = misogynist movement" narritive.](http://i.imgur.com/JQhJkgS.jpg)

* Literally Who's dox was posted on a newly created board on 8chan. Since Hotwheels was asleep, it couldn't be deleted. [In response, GamerGaters and 8chan-users teamed up to flood the board, burying the dox under a huge pile of shitposts.](http://i.imgur.com/TX6m0HK.jpg)

* [GG supporter threatened and advised to leave home by authorities.](https://archive.today/76xR6)

* Out of all possible people and organizations, [Raspberry Pie comes out in opposition to GamerGame, voicing support for #StopGamerGate.](https://archive.today/JW5lq)

* Buzzfeed publishes pro-GamerGate article: [8 Images That Show What #GamerGate Is Really About](https://archive.today/JNQYr)

* According to Utah State Today, [the threats against Anita Sarkeesian were a hoax. She cancelled the talk because she "was concerned about the fact that state law prevented the university from keeping people with a legal concealed firearm permit from entering the event. University police were prepared and had a plan in place to provide extra security measures at the presentation."](http://www.usu.edu/ust/index.cfm?article=54179)

* [Host of Huffington Post Live claims to have been doxxed by "not GamerGate".](https://archive.today/UJXsU)

* [PUBLICIST ANON warns of coming massive PR attack on gamers supporting gamergate within the next 24 hours](https://archive.today/MD7PU). A massive, co-ordinated effort across dozens of websites, news channels and social media outlets is alleged to be in planning, to be unleahsed over the weekend. Bot and shill accounts are said to be ready to be deployed. Their message will be to "Stop Gamergate" at all costs. 

* Anti-GamerGate articles of the day: [The Guardian](https://archive.today/kX1br), [CBC news](https://archive.today/3fuPb), [MotherJones](https://archive.today/zBJTX), [New Statesman](https://archive.today/ICol5)


### Oct 15th (Wednesday)

* Another Huffington Post Live on GamerGate just aired. [Watch the VOD here](https://www.youtube.com/watch?v=Nnuiie9zttU). The guests were [Georgina Young](https://twitter.com/georgieonthego), [Jennie Bharaj](https://twitter.com/jenniebharaj), and [Jemma Morgan](https://twitter.com/ShuluuMoo).  
tl;dw: All three of them were pro-GamerGate and they pretty much destroyed the "GamerGate = misogynist movement" narritive.

* [Boogie2988 was doxxed and received death threats.](https://archive.today/WWyTp)

* Anti-GamerGate articles of the day: [Washington Post](https://archive.today/KLcv0), [BBC](https://archive.today/bcNQT)

* TotalBiscuit continues going nuclear on twitter, posting multiple TwitLongers:  
[1](http://www.twitlonger.com/show/n_1s4nmr1), [2](http://www.twitlonger.com/show/ngscie), [3](http://www.twitlonger.com/show/n_1sd042v), [4](http://www.twitlonger.com/show/n_1sd058s).

* [An indonesian Botnet is being used to push the anti-GamerGate hashtag #StopGamerGate.](http://i.imgur.com/bLwwRzf.jpg) Ignore and do not engage.  
Ironically enough, [the Botnet also seems to be used by none other than the ISIS](https://pbs.twimg.com/media/Bz-mU_ACUAApdhp.png:large).

* [historyofgamergate.com](http://www.historyofgamergate.com/kotaku-in-action.html) founded by [@mudcrabmerchant](https://twitter.com/mudcrabmerchant), a site dedicated towards documenting the history of GamerGate.

* [BBC Radio podcast with Brianna Wu and Erik Kain.](http://downloads.bbc.co.uk/podcasts/worldservice/business/business_20141015-0100a.mp3)

* [TYFC releases a statement "address the issues of the project and our supporters"](http://thefineyoungcapitalists.tumblr.com/post/100084170915/on-feminism), reaffirming their support for GamerGate.


### Oct 14th (Tuesday)

* The ride never ends: [CNN](https://www.youtube.com/watch?v=VSP_7fsdYPI) (\[TW\]: direct link!) and [BBC](https://archive.today/9n8iI) made a reports on "death threats against female game developer", spinning the narritive into the direction of "GG is a misogynist movement" once again.

* Stream is over now, watch the VOD [here](https://www.youtube.com/watch?v=wNKvF5jOXUk). TL;DW: GamerGate wasn't even really talked about much, Host was biased towards Brianna Wu's narrative, Hotwheels BTFO'd her pretty hard.  
~~[Huffington Post Live will be talking about GamerGate today.](http://live.huffingtonpost.com/r/segment/gamergate-and-sexism-and-misogyny-in-video-game-culture-/5437df2b78c90a43d9000be8) Guests are Erik Kain (Forbes), Fredrick Brennan AKA Hotwheels (8chan admin), and Brianna Wu (Giant Spacekat).  
[Meanwhile, Literally Who is denied from stepping back into the spotlight by host Ricky Camilleri.](https://archive.today/IXjg0) As you can imagine, [this made a lot of anti-GG'ers very upset.](http://i.imgur.com/vYlskHw.jpg)~~

* [Boogie2988 has been banned from NeoGAF for supporting GamerGate.](https://twitter.com/Boogie2988/status/521787286783279105) Article on this by TheRalphRetort [here](http://theralphretort.com/disgraceful-neogaf-finally-bans-peaceful-boogie2988/).

* [The Michigan Economic Development Corporation (MEDC) pulls their ads from Gamasutra!](https://archive.today/MaAIn)

* Big surprise: [SJW rhetoric in Borderlands: The Pre-Sequel.](http://i.imgur.com/S3gQxo5.jpg) [KotakuInAction recommends a boycott.](http://www.reddit.com/r/KotakuInAction/comments/2j5dfx/why_boycotting_borderlands_presequel_is_so/)

* Aaaand another one: [Vice publishes next anti-GamerGate article.](https://archive.today/MFCAL) The headline has to be seen to be believed.

* [Sargon of Akkad's youtube channel is now being filtered by the Automoderator bot on reddit.](https://i.imgur.com/DUOnLZ1.png)


### Oct 13th (Monday)

* [BBC World Service Radio talks about #GamerGate](http://downloads.bbc.co.uk/podcasts/worldservice/business/business_20141014-0100a.mp3) (Starts 17:30, Ends 29:00)  
Hosted by [Roger Hearing](https://twitter.com/rdhearing), featuring [Ben Gilbert of Engadget](https://twitter.com/RealBenGilbert), [Shikha Sood Dalmia](https://twitter.com/shikhadalmia) and [Simon Littlewood](https://twitter.com/Lasalvagie).

* [The Reid Report, broadcast on MSNBC, reports on GamerGate.](https://twitter.com/TheReidReport/status/521722414908973056) Unfortunately, only anti-GG'ers were interviewed, and used this opportunity to spin the narrative in their favor, discrediting GamerGate as a movement of "misogynistic nerds" again. 8chan was mentioned as well. You can check out the VOD [here](https://www.youtube.com/watch?v=NMtLPCBIfFY) and [here](https://mega.co.nz/#!JJECzLSR!gP-vpvwFkGOEXQRrjp_iWaBBrMzm72ASz7ShBeGpUvI).

* [Boogie2988 talks about his true feelings regarding GamerGate, and has "a nervous breakdown".](https://www.youtube.com/watch?v=1FnrHhUJMxc) TL;DW: He's more supportive of GamerGate now than ever.

* [Polygon gives Bayonetta 2 a 7.5, criticising "blatant over-sexualization".](https://archive.today/UGKCS) Compare this to [the "sexism" in GTA V, which is okay totally with them and got a 9.5.](http://i.imgur.com/LPf5gWP.png)

* ["I've received thousands of tweets [...] people would like me to be killed." - John Walker of RPS 2014. (Spoiler: He lied.)](http://i.imgur.com/Y5vsEyp.jpg)

* [And another one, this time by The Guardian again, accusing GamerGate of having a "vicious right-wing swell".](https://archive.today/yv4dZ) 

* [Next anti-GamerGate piece published by a Gawker asset, this time Jezebel.](https://archive.today/OE4Kq)


### Oct 12th (Sunday)

* [Best Buy responds to a Disrespectful Nod email, voicing their understanding for GamerGate.](http://i.imgur.com/xKke2hW.jpg)


### Oct 11th (Saturday)

* Two articles on GamerGate published by the Financial post, [one being neutral](https://archive.today/tH9LK), the [other one strictly anti-GG.](https://archive.today/5eFSK)


### Oct 10th (Friday)

* [Game industry vet of 12 years interviewed by The Escapist regarding GamerGate.](http://www.escapistmagazine.com/articles/view/video-games/gamergate-interviews/12391-Glaive-GamerGate-Interview)

* [The Escapist publishes statements of 15 developers regarding their stance on GamerGate.](http://www.escapistmagazine.com/articles/view/video-games/features/12383-Game-Developer-GamerGate-Interviews-Shed-Light-on-Women-in-Games)  
tl;dr: 7 are in support, 7 are uncertain and 3 are anti-GamerGate.

* [Even more GameJournoPro list emails leaked by Milo. Journalists joking about paid reviews and Phil Fish.](http://www.breitbart.com/Breitbart-London/2014/10/10/GameJournoPros-joking-about-paid-reviews-and-mocking-Phil-Fish)

* [A former writer of The Verge threatens to "punch the first person who says GamerGate".](http://theralphretort.com/former-verge-writer-threatens-gamergate/)

* [Yet another anti-GamerGate article published by a Gawker asset.](https://archive.today/ncugC)

* [Robert Ohlen, the CEO of Dreamhack, calls GamerGate supporters "retards".](https://archive.today/OXN6U) He later deletes the tweet and [issues an apology.](https://archive.today/RW9Yg)

* ["Neutrality disputed" tag re-added to the biased GamerGame article on Wikipedia.](https://archive.today/P0jKv) Meanwhile, [more drama erupts over anti-GamerGate admin Ryulong.](https://archive.today/kfu4C).


### Oct 9th (Thursday)

* [Well-written and neutral article on GamerGate published on Real Clear Politics](http://www.realclearpolitics.com/articles/2014/10/09/the_gender_games_sex_lies_and_videogames_124244.html)

* ~~[The GamerGate thunderclap is now at a social reach of 1,680,100 and about to end in 3 hours!](https://www.thunderclap.it/projects/17127-gamergate)~~  
[ThunderClap](https://www.thunderclap.it/projects/17127-gamergate?locale=en) strikes, [#GamerGate hits its highest point yet, reaching almost 70k tweets](http://a.pomf.se/ekmupn.png).

* [The Verge publishes a new anti-GamerGate article, in which the author demands people to drop their support for GamerGate.](https://archive.today/ebuWC)

* [Christina H. Sommer's Husband, Frederic Sommers, has passed away.](https://twitter.com/davidfrum/status/519308429098098688) In response, Milo Yiannopolous [hosts a condolence book](http://www.dearbasedmom.com), in which GamerGate supporters voice their sympathies for her tragic loss. We've also [sent her some flowers.](https://twitter.com/CHSommers/status/519978273837973505)

* ["Neutrality disputed" tag removed from the still extremely biased Wikipedia article. Also, it is no longer flagged for deletion.](https://archive.today/zykvj)


### Oct 8th (Wednesday)

* [Slate publishes an article about a lack of civility on Twitter, specifically calling out those attacking gamers and #Gamergate supporters .](http://www.slate.com/articles/technology/technology/2014/10/twitter_is_broken_gamergate_proves_it.html)  

* [George Reese (of Dell) deletes his tweet](https://twitter.com/GeorgeReese/status/519095004338593792) in which he compared GamerGate supporters to the ISIS. [Here](https://archive.today/X6Olx) is an archive.today'd backup of the tweet.  

* [Someone attempted to "hack" Sockarina's accounts.](https://twitter.com/Rinaxas/status/519764678499520512)  

* [An AMA with Milo Yiannopoulos is currently ongoing at reddit.](https://www.reddit.com/r/KotakuInAction/comments/2io2hf/i_am_milo_yiannopoulos_im_a_journalist_reporting/)  

* [Matthew Mitchum, designer of the webgame "Brunelleschi: Age of Architects", thanks #GamerGate for "[...] working to empower ethics in game journalism [...]" in said game's credits.](https://twitter.com/MultiAxisMatt/status/519389321032253440)  

* [The #GamerGate thunderclap is now at 2,494 supporters with a social reach of 1,454,028 people!](https://www.thunderclap.it/projects/17127-gamergate)


### Oct 7th (Tuesday)

* [Jim Sterling exposes paid streams and Let's Plays of Shadow of Modor, orchestrated by a PR company called "Plaid Social". Includes shady "contracts" and "guidelines" and all that good stuff.](http://www.escapistmagazine.com/videos/view/jimquisition/9782-Shadow-of-Mordors-Promotion-Deals-with-Plaid-Social)

* [Episode Three of Radio Nero released.](https://soundcloud.com/radio_nero/series-1-episode-3-gamers-vs-gamr)

* [The biased Wikipedia-article on GamerGate is now flagged for deletion and a lack of neutrality.](https://archive.today/0bVe0)


### Oct 6th (Monday)

* Dellgate: [George Reese, Executive Director of Cloud Computing at Dell](https://archive.today/ghgpj) calls #GamerGate ["the technology world's ISIS"](https://archive.today/X6Olx).  
TotalBiscuit retorts: [1](https://archive.today/TtbXG), [2](https://archive.today/iNNiG), [3](https://archive.today/cY16v), [4](https://archive.today/e4l7T), [5](https://archive.today/KXEBp), [6](https://archive.today/9yjZ4)

* [TotalBiscuit declares his support for #GamerGate](https://archive.today/dWzOP), and [goes](https://archive.today/cRNNb) [completely](https://archive.today/9KaiX) [nuclear](https://archive.today/8oMdU) [on](https://archive.today/XvbLT) [Twitter](https://archive.today/qgvdk), [Ȉ̴̢̯̗̠͓͖̦̤͖̗̖̝̥̜͇͈̹͙̘̞̔͂͑̉̆͊ͭ̀͊̄͒̈̔ͦT̨̳͍͈̦̮͂̐̀͌ͯ̉̾͌͗͌̏̉̈́̆́́̚͢͜'͇̥͖̪͚̱͙̮̣̿ͩͯ͑̀͘͘͝ͅͅS̴̛̮̤̩͇̝͈̥̹̭̫̪̬̋̋̀ͨ͋̍̒̊̌͋̿̌̉ͫ͊ͤ́ͧ͘͠ͅ](https://archive.today/7q34E) [H̩̗̤̥̮̳̖̞̗̲̱̤̱̟͕̖̳ͣͫ͋̿̓ͮͤ͗̆͊͐͜͞ͅA̧̠̮̼͇̬̟̤͚͔͈̰̪͉ͨͣ͑͛ͧ͒̍ͥ̐̆̿͋̆̉̋ͩͤ̉̕P̴̷̧̛̬̺͈̭͎͚̯̭̳̳̘͕͖ͯ̿̇̇̔̋́ͅP̧̲͕̣͔̘̘̝͉̹̥̳̦̳̞ͦ̒̈̾̅͠͠E̴̷̝̹͇͔̬̥̞̘͉͙̳̬͎̱̮͋̿͑̈́͋͘͡Nͣ̓̌ͫ̾̐̽͆͊̑͏̡̢̱͓̩̪̩̕͠I̶̛̍̅͌ͬ̊̏͌̆̚҉̺͇̮͇̱̲̠̺̥͇̝̰̟N̸͓͚̭̺̹̦̯̰͙͚̞̙̘̬̄̿ͨ͆̇ͪ̒͝ͅG̩̩͇̠̏͋̄̿ͦ̆͛͐̕͝͡](https://archive.today/9wn52).

* Turns out we were mislead, [GitLab has taken down the repository](https://archive.today/6ALlD) claiming that we are ["spamming people" and carrying out a "systemic harassment of people (especially women)"](https://archive.today/d1Sd7).
[The Fine Young Capitalists chips in to the #GamerGate outcry against GitLab](https://twitter.com/TFYCapitalists/status/519131952591691776) [Archived Link](https://archive.today/b3gxg).

* [SouthPark](https://twitter.com/SouthPark) announces [their new episode, "The Cissy"](https://twitter.com/SouthPark/status/519193892797698048), [rumor has it](https://twitter.com/Int_Aristocrat/status/519203177975906304) [it will be parodying SJWs](https://twitter.com/Int_Aristocrat/status/519203484961222656).

* [Niche Gamer (a small pro-GG site) gets a review copy of Alien Isolation! Based SEGA.](https://twitter.com/thenichegamer/status/519263300262309888)


### Oct 5th (Sunday)

* [#GamerGate Thunderclap started.](https://www.thunderclap.it/projects/17127-gamergate) It is [currently the #1 trending campaign.](http://a.pomf.se/detoir.png) and at 300% of it's current goal! Go boost it even higher before October 9th! (Its end date)

* <del>After Gitlab staff [proved themselves](https://archive.today/nqNqr) to be [pretty based](https://archive.today/2he9I), we've decided to permanently migrate to Gitlab.</del>


### Oct 4th (Saturday)

* [Erik Kain announces a Livestream on GamerGate with Greg Tito, TotalBiscuit and Janelle Bonanno.](https://twitter.com/erikkain/status/518531324215570435)  
<del>Date: This monday, 2pm PST.</del> It has been recorded: [Full recording here](https://www.youtube.com/watch?v=rmosgPNXmNc).

* [Apparently, InternetAristocrat has been doxxed.](https://twitter.com/Int_Aristocrat/status/518629853260558336)


### Oct 3rd (Friday)

* [Intel releases statement on why they've decided to pull their ads from Gamasutra.](http://newsroom.intel.com/community/intel_newsroom/blog/2014/10/03/chip-shot-intel-issues-statement-on-gamasutra-advertising) [Image](http://33.media.tumblr.com/b88c9b60d4f43ec3bf5c98b03335f59d/tumblr_ncwarttyWQ1sm2yjco1_1280.png)

* Github unilaterally disables the GamerGateOP repository, and all related forks. ~~As you can see, we've moved to Gitorious instead.~~ ~~Gitlab~~ We eventually moved to Gitgud.  
[This is the email we've received from github.](http://a.pomf.se/cozqye.png)  
[Appearently a single employee was responsible.](http://a.pomf.se/xdgspe.png)
[Gamers attempting to contact the Github complaints department are allegedly being doxxed](http://www.twitlonger.com/show/n_1sce3fa)
[Pipedot story and discussion of the Event](https://pipedot.org/story/2014-10-04/github-staff-jake-boxer-disables-gamergate-operation-disrespectful-nod-repository)
Combined with the DDoSing of 8chan, Gamers' consumer email campaign is temporarily distrupted.

* [8chan hit by a DDOS attack.](https://twitter.com/infinitechan/status/518146964358459392) The site is back up now, [but the attacks are ongoing.](https://twitter.com/infinitechan/status/518202045229453313)


### Oct 2nd (Thursday)

* Barrage of Articles appear from Oct 1st to 4th, following Intel's decision to pull ads from Gamasutra.
Several sites accuse Intel of supporting a "harrassment"  campaign.
Games journalists appear to notice the existence of Gamers' consumer email campaign for the first time.

#### Selection of articles
http://techraptor.net/content/gamasutra-intel  
http://www.nichegamer.net/2014/10/intel-pulls-ads-from-gamasutra-in-the-wake-of-gamergate/  
http://adland.tv/adnews/intel-has-gamers-inside-pulls-advertising-gamasutra/251869514  
http://bits.blogs.nytimes.com/2014/10/02/intel-pulls-ads-from-site-after-gamergate-boycott/  
http://www.gamesindustry.biz/articles/2014-10-02-intel-pulls-ads-from-gamasutra-in-response-to-gamergate  
http://www.dailydot.com/geek/intel-pulls-ads-from-gamasutra/  
http://boingboing.net/2014/10/01/gamergateintel.html  
http://adland.tv/adnews/intel-has-gamers-inside-pulls-advertising-gamasutra/251869514  
http://www.themarysue.com/intel-gamergate/  
https://www.yahoo.com/tech/intel-buckles-to-anti-feminist-campaign-by-pulling-ads-98981250049.html  
http://www.brightsideofnews.com/2014/10/02/gamergate-intel-faces-backlash-for-pulling-gamasutra-ads/  
http://www.valuewalk.com/2014/10/intel-corporation-pull-ads-from-gamasutra/  
http://www.thefrisky.com/2014-10-03/intel-caved-to-anti-feminist-campaign-pulls-ads-from-gaming-site-gamasutra/  
http://arstechnica.com/gaming/2014/10/intel-folds-under-gamergate-pressure-pulls-ads-from-gamasutra/  
http://www.theverge.com/2014/10/2/6886747/intel-buckles-to-anti-feminist-campaign-by-pulling-ads-from-gaming  


### Oct 1st (Wednesday)

* [Intel pulls their ads from Gamasutra!](http://i.imgur.com/h5WqpM1.jpg) Confirmed by, [IntelGaming's twitter](https://twitter.com/IntelGaming/status/517494247939784704). [Tweet later removed](https://archive.today/PTOQY)


## September 2014


### Sep 30th (Tuesday)

* [Second episode of Radio Nero released.](https://soundcloud.com/radio_nero/series-1-episode-2-professional-failures)

* [Cracked rejects pro #GamerGate article by Jamie Butterworth.](http://www.twitlonger.com/show/n_1sc93l4)

* [Patreon CEO considers taking down The Sarkeesian Effect due to "offensive content".](http://i.gyazo.com/527e647085f0df7f8d5bb3a190ce0de5.png) Interestingly enough, it seems like [he also held a talk at XOXO.](https://pbs.twimg.com/media/By2mS1BCIAACj_C.jpg). 


### Sep 29th (Monday)

* [Thunderf00t's suspension from Twitter is lifted](https://twitter.com/thunderf00t/status/516715454849900545)

* [APGNation interviews Willian Usher, the man behind the GameJournoPro leaks.](http://apgnation.com/archives/2014/09/29/7694/breaking-the-chain-an-interview-with-william-usher)

* [Total Biscuit](https://twitter.com/Totalbiscuit) tweets [a TwitLonger critiquing The Verge's attempts to inject gender politics into articles about video games](http://www.twitlonger.com/show/ngiujr) [Archive Link] (https://archive.today/I7nVl).


### Sep 28th (Sunday)

* [Sockarina reports online Harrassment. Reaffirms support for #Gamergate](https://www.youtube.com/watch?v=sCr5f9GA2P4)

* KingOfPol claims to have been doxxed by a 4chan mod on /pol/. **Avoid posting on 4chan for now!**  
Tweets: [1](https://twitter.com/inawarminister/status/516180165332701186), [2](https://twitter.com/Kingofpol/status/516183675630014464), [3](https://twitter.com/Kingofpol/status/516183975132672000), [4](https://twitter.com/Kingofpol/status/516184214728114176),
[5](https://twitter.com/Kingofpol/status/516184410807623680)


### Sep 27th (Saturday)

* [27th September - New Video from InternetAristocrat detailing a timeline of GamerGate since the begining of September] (https://www.youtube.com/watch?v=_dbi-8rPShE)

* [Matt of TFYC thanks everyone involved for making their IndieGoGO campaign a success and announces that he will step down from the project.](http://thefineyoungcapitalists.tumblr.com/post/98525462265/on-endings)

* [The Spectator](http://en.wikipedia.org/wiki/The_Spectator) releases [an article in favour of GamerGate](http://www.spectator.co.uk/columnists/james-delingpole/9322382/why-i-love-grand-theft-auto-v-and-the-feminazis-hate-it/).


### Sep 26th (Friday)

* [Wikipedia admin shuts down GamerGate discussion.](http://i.imgur.com/FdkVvb4.jpg) Unfortunately, the biased and one-sided anti-GamerGate article will stay as it is for now. Just as a reminder: This is the first thing people see when they google "GamerGate". Something *needs* to be done about this.

* [Sargon of Akkad](https://twitter.com/Sargon_of_Akkad) releases his first video in a series on ["The Feminist Ideological Conquest of DiGRA"](https://www.youtube.com/watch?v=28D6_8KuIpc).


### Sep 25th (Thursday)

* [VoD of latest InternetAristocrat stream.](https://www.youtube.com/watch?v=yS8bjnMlR5A)


### Sep 24th (Wednesday)

* ["We are Gamers. We are alive." as read by Big Man Tyrone.](https://www.youtube.com/watch?v=Q2v_Anr5cbs)

* [GamerGate related content is being deleted from archive.org.](https://twitter.com/RogueStarGamez/status/515022498203983872) Move to [archive.today](https://archive.today/) instead.


### Sep 23rd (Tuesday)

* [8chan.co's full source code is now publicly available](https://twitter.com/infinitechan/status/514519596645486593) under [slight modifications of the MIT and X11 license](https://github.com/ctrlcctrlv/8chan/blob/master/LICENSE.md), [make a Github account](https://github.com/) and please [star and help contribute to the repository](https://github.com/ctrlcctrlv/8chan)!

* [The #GamerGate and #notyourshield hashtags have reached over a million tweets combined!](https://pbs.twimg.com/media/ByPfEm0IEAEIKHc.png:large) Kudos to everyone involved.

* [New Breitbart article on how Ben Kuchera tried to damage the image of Stardock's CEO](http://www.breitbart.com/Breitbart-London/2014/09/23/How-sloppy-biased-video-games-reporting-almost-destroyed-a-CEO)


### Sep 22nd (Monday)

* New info regarding GameJournoPros published:  
["'They're On To Us': Gaming Journalists Respond to Critics in Newly Revealed GameJournoPros Emails"](http://www.breitbart.com/Breitbart-London/2014/09/22/They-re-on-to-us-gaming-journalists-respond-to-their-critics-in-series-of-new-GameJournoPros-emails)  
["The List: Every Journalist in the GameJournoPros Group Revealed"](http://www.breitbart.com/Breitbart-London/2014/09/21/GameJournoPros-we-reveal-every-journalist-on-the-list)  
[Summary of the emails on Milo Yiannopoulos' blog](http://yiannopoulos.net/2014/09/22/new-gamejourno-pro-leak-the-email-summaries/)

* [Series 1, Episode 1 of Radio Nero](https://soundcloud.com/radio_nero/radioneroep1_1) comes out! Featuring [Adam Baldwin](https://soundcloud.com/radio_nero/radioneroep1_1), [Christina Hoff Sommers](https://twitter.com/CHSommers) and the [Internet Aristocrat](https://twitter.com/Int_Aristocrat)!

* Livestream by [@NotYourShield](https://twitter.com/NotYourShield): [The Women Of #GamerGate](https://www.youtube.com/watch?v=cjdiC2ednok).

* [KotakuInAction livestream](https://www.youtube.com/watch?v=SB8d5H_DB1w), features people from the Industry, [Hatman](https://twitter.com/thehat2) has probably a full list of who was in there.

* [New MundaneMatt video](https://www.youtube.com/watch?v=dTxf6W3kdwU), Mundane Matt knows someone who is on the [Mailinglist](http://www.breitbart.com/Breitbart-London/2014/09/21/GameJournoPros-we-reveal-every-journalist-on-the-list).

* [8chan.co is becoming a partner of 2ch (yes, that 2ch!).](https://twitter.com/infinitechan/status/514108117731516416)

* [KingofPol streams again](https://www.youtube.com/watch?v=OvwQoy1kYag), guests include [Sargon of Akkad](https://www.youtube.com/channel/UC-yewGHQbNFpDrGM0diZOLA), Roguestar, [Milo](https://twitter.com/Nero) of Breitbart News, Anti-#GamerGate member Kos, [Todd from Stralya (CounterTunes)](https://www.youtube.com/channel/UCZ908m6ZCkeyFPWSymp2riA) and [Synthovine](https://twitter.com/Synthovine) of KotakuinAction.  
[Starting at 02:32:56](https://www.youtube.com/watch?v=OvwQoy1kYag#t=2h32m56s), we hear from the Anti-#GamerGate side by Kos.

* [Pastebin from Little Mac (or Bob)](http://pastebin.com/VtzAxPXf) on how for instance he was heavily encouraged to contribute to a kickstarter of Anita Sakreesian or looses his job as well banned from Mighty No. 9 as well kickstarter rewards. He also gives some advice to the movement.  
Source: [Tweet from Hatman](https://twitter.com/TheHat2/status/514197489751429121) and [Livestream](https://www.youtube.com/watch?v=SB8d5H_DB1w).


### Sep 21st (Sunday)

* [TFYC voting has ended; Afterlife Empire is the winner.](http://thefineyoungcapitalists.tumblr.com/post/98028000740/afterlife-empire-recieve-the-most-votes)

* [Titanium Dragon (a pro-GamerGate Wikipedia editor) has been banned from editing GamerGate and related articles.](http://i.imgur.com/gYvqWOA.jpg)

* [Guest video by TheInvestigamer on GamerGate posted on TheAmazingAtheist's channel (600k subs)](https://www.youtube.com/watch?v=YCExXie1XB4)


### Sep 20th (Saturday)

* [The Escapist is suffering from a DDoS attack. Specifically, the attackers are targeting the GamerGate thread.](https://twitter.com/archon/status/513365103329423360)

* [TotalBiscuit talks about recent events.](https://www.youtube.com/watch?v=2fYSOPifbUk) Seems like he's on the verge of losing his neutral stance...

* [All GamerGate discussion has been banned from the TvTropes forums](https://pbs.twimg.com/media/ByCibyfCAAACyuQ.png:large), [tweet by IA](https://twitter.com/Int_Aristocrat/status/513588957473292290).

* [John Carmack BTFOs a SJW during the Oculus Connect Keynote.](https://www.youtube.com/watch?v=vzmbW4ueGdg)

* [Someone made an offer to to buy 8chan.](http://i.imgur.com/eKbYi7v.jpg)

* [Someone was banned from viewing the Mighty No. 9 twitter account for supporting GamerGate.](https://pbs.twimg.com/media/ByBZB2eCYAEkXKR.png)

* [Sargon of Akkad explains why GamerGate is so important to him.](https://www.youtube.com/watch?v=yZI2IRbSKts) His explanation will probably resonate with most of us.


### Sep 19th (Friday)

* [Another Anti-GamerGate piece by Washington Post.](https://archive.today/MhNiP)

* [Polar Roller@j_millerworks](https://twitter.com/j_millerworks), [creator of #NotYourShield](https://twitter.com/IonCaron/status/508542963832860672), is [fired from his job due to complaints from SJWs](https://twitter.com/j_millerworks/status/513198848542781440).

* [All of the GameJournoPros were dumped. Censored version with no personal info here.](http://yiannopoulos.net/2014/09/19/gamejournopros-zoe-quinn-email-dump/)

* [Someone ("a group") is trying to hack into various accounts of Matt (TFYC) again.](https://twitter.com/TFYCapitalists/status/513048546858532864)

* [Milo Yiannopoulos](https://twitter.com/Nero) has a public fall out with [Liana Kerzner](https://twitter.com/redlianak) (Polygon Contributor) over [possible screenshots of Ben Kuchera's mails to GameJournoPros](https://twitter.com/Nero/status/513016521753628672).  
Conversation: [1](https://twitter.com/Nero/status/513053224400859136), [2](https://twitter.com/redlianak/status/513057496064589825), [3](https://twitter.com/Nero/status/513059027161735168), [4](https://twitter.com/Nero/status/513059688024645633), [5](https://twitter.com/Nero/status/513067602709864449)

* [PressFartToContinue was banned from twitter.](https://twitter.com/fartchives/status/513041035862503424)

* [Out of all possible sites, freaking FunnyJunk makes an announcement saying that GamerGate discussion is allowed.](http://www.funnyjunk.com/gamergate+sjw+discussion+allowed/text/5299181/) *Just to make this more clear: Discussion not allowed on 4chan. Discussion allowed on Epicmaymay-Paradise. I'm at a loss of words again.*


### Sep 18th (Thursday)

* [4chan Censors #Gamergate threads. Moot speaks up, confirming that GamerGate discussion is no longer allowed on /v/.](http://boards.4chan.org/v/thread/264227139/regarding-recent-events) ~~*moot removed the sticky, luckily there's still an archived copy of it*~~ *Update: Now the sticky is back. It's a new thread with the same text as before. No idea what that is all about.*   
[Migrate](http://a.pomf.se/khegwn.png) to [/burgers/](https://8chan.co/burgers/) or [/v/](https://8chan.co/v/) on 8chan instead.  
As expected, most people are not all too happy with that announcement. [/v/ is being flooded in protest,](http://i.imgur.com/WorR3KO.jpg) [as are /sp/](https://i.imgur.com/iRLPyap.png) and some other boards.  

* [Livechan developer adds a /v/ board](http://a.pomf.se/nbmkky.png) for [Video Game discussion](https://livechan.net/chat/v) and [GamerGate + NotYourShield](https://livechan.net/chat/v#%23GG%2B%23NYS).

* [A Conversation with Polygon Contributor Liana Kerzner about #GamerGate (Sargon of Akkad and Liana Kerzner)](https://www.youtube.com/watch?v=QuHfu3W1BGE)

* Mod of [subreddit KotakuInAction](http://www.reddit.com/r/KotakuInAction/) ([Hatman](https://twitter.com/thehat2)), [hosts a LiveStream](https://www.youtube.com/watch?v=0FlVaHXqaQg) featuring [MundaneMatt](https://twitter.com/MundaneMatt), [TotalBiscuit](https://twitter.com/Totalbiscuit), [Reddit/r/gaming Mod](http://www.reddit.com/user/synbios16), [Lo-Ping](https://twitter.com/GamingAndPandas), [Devi Ever](https://twitter.com/deviever), [NotYourShield](https://twitter.com/NotYourShield), [Andrew Otton (techraptor writer)](https://twitter.com/OttAndrew) and many more (check the [video description!](https://www.youtube.com/watch?v=0FlVaHXqaQg)).

* [KingfPol](https://twitter.com/Kingofpol) hosts a [Stream of Happenings at hitbox](http://www.hitbox.tv/kingofpol), featuring a Goodgamers.us writer, [Matt (TFYC Founder)](https://twitter.com/TFYCapitalists), [Milo Yiannopoulos](https://twitter.com/Nero), [Adam Baldwin](https://twitter.com/AdamBaldwin) and many more.  
Recording of the stream has been [uploaded to youtube here](https://www.youtube.com/watch?v=1B1cvL1Fn4U).


### Sep 17th (Wednesday)

* [Moot shown to have attended XOXO, the place where Sarkeesian hold her "listen and beLIEve"-speech.](https://pbs.twimg.com/media/Bxsc_qeCMAAnvlO.png#_=_)

* Milo Yiannopoulos exposes "GameJournoPros", a secret mailing list on which several prominent gaming "journalists" "discuss what to cover, what to ignore, and what approach their coverage should take to breaking news".  
[His article with all the important information.](http://www.breitbart.com/Breitbart-London/2014/09/17/Exposed-the-secret-mailing-list-of-the-gaming-journalism-elite)  
[Milo confirms that more details are still unpublished.](https://twitter.com/Nero/status/512353477628940290)  

* [Summary of InternetAristicrat's stream. Read this if you don't know what to do next.](http://pastebin.com/KGwY37Fn)


### Sep 16th (Tuesday)

* GamerGate-related threads are being deleted and OPs banned from [/v/](https://pbs.twimg.com/media/Bxriv52CMAIeG-r.jpg) once again. The OP here on github is being autofiltered as well.  
[People are being banned/kicked from IRC.](http://i.imgur.com/dtzjFBr.png)  
[Appearently threads are being deleted because of "spamming".](http://i.imgur.com/n74rwJ4.png)  
[4chan's feedback page just 404'd.](http://www.4chan.org/feedback)  
[They aren't even bothering anymore with giving a proper reason for banning.](https://twitter.com/DignitasSunBro/status/511986066132598784)  
[More IRC logs.](https://pbs.twimg.com/media/BxrjxO2IQAAIsok.png:large)  
[Mods banned a bunch of German IP ranges, affecting people who have never even heard of GamerGate](https://www.youtube.com/watch?v=OcV2RCvPKvM).

* ["Are video games sexist?" by Christina H. Sommers. Trigger Warning: BTFO's SJWs to hilarious degrees.](https://www.youtube.com/watch?v=9MxqSwzFy5w)

* [Polygon is at it again, this time attempting to discredit C.H. Sommers as a "conservative" among other mind-bending things.](https://archive.today/BQbD7)

* [Richard Dawkins posts a tweet in support of C.H. Sommers.](https://twitter.com/RichardDawkins/status/512110699435524096)

* [Thunderf00t was suspended from twitter.](https://www.youtube.com/watch?v=6a4vaZy0a18)

* [Christopher Arnold, developer of the indie game "Freak", explains why he is in support of GamerGate.](http://www.twitlonger.com/show/n_1sakrnh)

* [Some thoughts by InternetAristocrat on how to proceed with GamerGate. Also, he announced a livestream. 9/17/2014 at 6pm central standard time.](http://pastebin.com/tAppaZjy)


### Sep 15th (Monday)

* [Infographic for people coming to GamerGate from the WikiLeaks tweets](http://a.pomf.se/zbgeak.png)

* [Julian Assange on reddit censorship](http://a.pomf.se/qwmnxm.png)

* [GamerGate gets a supportive shout-out from WikiLeaks!](https://twitter.com/wikileaks/status/511727048931282944)


### Sep 14th (Sunday)

* [goodgamer.us interviews TFYC](http://www.goodgamers.us/2014/09/14/interview-the-fine-young-capitalists/)

* [The wikipedia article on GamerGate is completely unbiased! (not)](http://i.imgur.com/pHt972J.jpg)

* [Yet another publication trying to discredit GamerGate, this time Cracked](https://archive.today/0gGBF)


### Sep 13th (Saturday)

* [LeoPirate Video on 9/11 Harassment of him by @a_girl_irl](http://youtu.be/vOoJuRYIRjM)

* [boogie2988 on the backslash he has received for supporting GamerGate (Img link Broken)](https://pbs.twimg.com/media/Bxf4yFfIQAELerF.png:large)

* **NOTE:** *Try to give this person the least amount of attention possible. Don't make any tweets about/directed at her, don't watch her videos, don't spread non-archived article about her, etc.*   
Anita Sarkeesian holds a speech at XOXO Festival and calls for a "Feminist Army". [Includes discrediting GamerGate as a conspiracy theory via cherry-picked tweets](http://i.imgur.com/ju4wdDc.jpg) all that stuff you would expect.  
[I can't make this shit up:](https://pbs.twimg.com/media/BxgTiF_IcAAYEEl.jpg) ["Listen and believe"](http://i.imgur.com/eVZTv7m.jpg)  
["A room full of mid-30s white people call discuss "oppression" and "diversity" in gaming"](https://pbs.twimg.com/media/BxftFbRIMAI_LGu.jpg:large)  
[BuzzFeed in full support of this](https://archive.today/oG0P9)  
[As is The Verge](https://archive.today/5UQjh)  


### Sep 12th (Friday)

* [techraptor interviews Daniel Vavra on GamerGate](http://techraptor.net/2014/09/12/interview-daniel-vavra/)

* [Ben Kuchera of Polygon refuses to speak with a transgender female game dev after hearing she is in support of GamerGate (Link broken)](https://twitter.com/deviever/status/510547381297758208)  

* [Gamasutra's Alexa rank continues to drop(Undated).](http://i.imgur.com/JScbNnU.png)  


### Sep 11th (Thursday)

* **TFYC SUCCESSFULLY FUNDED ON 09/11! GOOD JOB GUYS!**  
[Statement by TFYC](http://thefineyoungcapitalists.tumblr.com/post/97242984635/on-broken-phones-and-meeting-goals)  
[spark82 CONFIRMED AS THE HERO OF VIDYA](http://i.imgur.com/c1O6pFf.jpg)  

* [UniLever confirms they will no longer advertise with Polygon](https://twitter.com/SHG_Nackt/status/510012273830932480)

* [Interview with Dr. Greg Lisby, regarding ethics in journalism.](https://www.youtube.com/watch?v=4-7RLxrsJ04)

* [New game news website goodgamers.us launched: "All of the gaming news, none of the bullshit"](http://www.goodgamers.us/)  

* [New Zealand Developer speaks out, "...GamerGate's been a long time coming."](https://archive.today/ix6Wv)

* [TFYC were banned from NeoGAF.](https://pbs.twimg.com/media/BxQDNcNCEAEAQqu.jpg:large)

* [Yet another anti-GamerGate article by The Telegraph](https://archive.today/e6Jub)

* [Milo Yiannopoulos](http://en.wikipedia.org/wiki/Milo_Yiannopoulos)[@Nero](https://twitter.com/Nero) makes a [thread on 4chan](https://twitter.com/Nero/status/510221194164187136) that reaches [2660 replies and 795 images](http://a.pomf.se/vsybnt.png).   [Jadusable](https://www.youtube.com/user/Jadusable)[@AlexanderDHall](https://twitter.com/AlexanderDHall) also [posts in the thread](https://twitter.com/AlexanderDHall/status/510232373364285441).  
Archived thread: https://archive.moe/v/thread/262952626/

* [Davis Aurini questions as to when/if Anita Sarkessian contacted authorities about her twitter harrassment](http://www.staresattheworld.com/2014/09/anita-sarkeesian-fabricate-story-contacting-authorities/)


### Sep 10th (Wednesday)

* [GamerGate is now spreading to Japan as well!](http://a.pomf.se/dcgvrw.png)

* [Very detailed pastebin showing how the Gamer+ clique (Silverstring, DiGRA, Gamasutra, among others) is connected to one another.](http://pastebin.com/PmdSbPHN)

* [Joe Rogan (1.41M followers) is interested in covering GamerGate.](http://i.imgur.com/PqZyj4v.png)

* [Jason Schreier's "justification" as to why he is not writing about TFYC.](http://www.twitlonger.com/show/n_1s9dufq)


### Sep 9th (Tuesday)

* [Polygon loses Scottrade as an an advertiser!](https://twitter.com/Chriss_m/status/509486352174694400)

* [Kotaku gone from the list of StackSocial affiliates!](https://stacksocial.com/business/affiliates)

* [APGNation's interview with TFYC, giving more details on how Zoe hindered them](http://apgnation.com/archives/2014/09/09/6977/truth-gaming-interview-fine-young-capitalists)

* [TotalBiscuit rambles about the gaming media and current events for 30 minutes](https://www.youtube.com/watch?v=e78JRIHRjC0)

* A bunch of articles trying to discredit #GamerGate as a misogynistic movement created by conspiring 4chan users were published in the past ~24 hours:  
[1. arstechnica: Chat logs show how 4chan users created #GamerGate controversy](https://archive.today/GC5MS)  
[2. Wired (Note: same author as the arstechnica article): How 4chan manufactured the #GamerGate controversy](https://archive.today/LTFr9)  
[3. The New Yorker: Zoe Quinn's Depression Quest](https://archive.today/nb14y)  
[4. Bustle: What Is "#Gamer Gate"? It's Misogyny, Under The Banner Of "Journalistic Integrity"](https://archive.today/UWsE3)  
[5. The Telegraph: Misogyny, death threats and a mob of trolls: Inside the dark world of video games with Zoe Quinn - target of #GamerGate](https://archive.today/vdsyJ)  

* [Another article trying to discredit GamerGate surfaced, this time voicing support for NeoGAF out of all sites.](https://archive.today/xbPE1)

* Der Spiegel, a once quite respected German news magazine, publishes an article heavily biased against GamerGate:  
[1. Scan of the article](http://i.imgur.com/QvFixou.jpg)  
[2. Translation by someone on leddit](http://reddit.com/r/KotakuInAction/comments/2fsxf3/der_spiegel_a_german_newspaper_printed_an_article/)  


### Sep 8th (Monday)

* ["#GamerGate is winning!!!": A pretty motivating video by MundaneMatt detailing how we actually *are* having an effect on gaming media.](https://www.youtube.com/watch?v=nENGp6d684I)

* Two supporters of #GamerGate doxxed; Zoe proceeds to tweet the dox around  
[1. Censored screencap of the dox](http://i.imgur.com/He7UCVW.png)  
[2. Screencap of Zoe's tweet](http://i.imgur.com/k96uLWF.jpg)  
[3. Zoe attempting to backpedal from the spreading of the dox](https://archive.today/lIn6s)

* Evidence of reddit admins/mods "censoring, reading personal messages, shadowbanning, altering content, and threatening people with charges of CP" surfaced:  
[1. Soundcloud link] (https://soundcloud.com/user613982511/recording-xm-2014)  
[2. Indiejuice article](http://indiejuice.tv/moderating-front-page-internet/)  
[3. One admin denying claims of censorship](http://a.pomf.se/ycliin.png)  
[4. Reddit in full damage control](http://a.pomf.se/chzjot.png)  
[5. /r/gaming mod trying to justify the deletion of a submission of TotalBiscuit's new video](http://puu.sh/bsC0f/132c0012be.png) 


### Sep 7th (Sunday)

* Medium.com publishes a narrative of #Gamergate rebutting Zoe Quinns claims of a harrassment conspiracy by #4chan:  https://medium.com/@cainejw/a-narrative-of-gamergate-and-examination-of-claims-of-collusion-with-4chan-5cf6c1a52a60

* IGF, INDIECADE AND POLYTRON BTFO'D ON STREAM BY LORDKAT - Alleged evidence for racketeering leaked (Later withdrawn):  
[1. Transcript of the stream](http://www.lordkat.com/igf-and-indiecade-racketeering.html)  
[2. Infograph](https://pbs.twimg.com/media/Bw-dYT_CIAActWR.jpg:large)  
[3. Video by ShortFatOtaku (Has been made Private)](https://www.youtube.com/watch?v=HM_Z5YTop7g)  


### Sep 6th (Saturday)

* Zoe lurks on 4chan IRC channels, makes logs, then post these logs on her twitter out of content under the label "GamerOverGate":  
[1. Video by MundaneMatt on this topic](https://www.youtube.com/watch?v=uv0Fg20i1iA)  
[2. Rebuttal of the IRC logs](http://i.imgur.com/cBv8Wx5.png)  
[3. Response to #GameOverGate](http://i.imgur.com/g385IJM.jpg)

* [VoD of InternetAristocrat's "shitaku" stream, in which he talks about GamerGate related stuff](https://www.youtube.com/watch?v=TpwyEq_m0zc)

* [Los Angeles Times write a Gamer+(Anti #GG) article](http://imgur.com/a/cDYtY)

* [#GamerGate and related stuff still increasing in activity on twitter](http://i.imgur.com/uzm5MUc.png)

* [Some dipshit spams Anita Sarkeesian's twitter with CP. Mundane Matt post video response saying #Gamergate supporters are not responsible](https://www.youtube.com/watch?v=hsDpX9WSXnc)


### Sep 5th (Friday)

* ["I believe that Kotaku writers are indeed entitled to pay into a game developer Patreon if that's what they need to do to access a developer's work for coverage purposes. They can even expense it." -Stephen Totilo](https://archive.today/ofdPK)  
Sometimes, I really wish I was making some of these happenings up.


### Sep 4th (Thursday)

* [Matt of TFYC on Honey Badger Radio](http://honeybadgerbrigade.com/radio/honey-badger-radio-quinnspiracy-2-electric-boogaloo/)

* Adam Baldwin and Internet Aristocrat arrange meeting at Radio Show by [EdMorrisey on ustream](http://www.ustream.tv/channel/the-ed-morrissey-show): http://a.pomf.se/nrawuw.png

* [InternetAristocrat and Adam Baldwin on The Ed Morrissey Show](http://www.ustream.tv/recorded/52292531)

* [Lawfag cancels his indiegogo campaign](https://www.indiegogo.com/projects/lawyers-against-gaming-corruption#activity) after hosting an [Ask Me Anything on Reddit](http://www.reddit.com/r/AMA/comments/2feuyz/attorney_representing_goodgamersus_ama/).  He is working on refunding all donations.

* [NOTE: now cancelled] Lawfag launches his indiegogo campaign, "Lawyers Against Gaming Corruption": http://igg.me/at/GamerGate/x/8571466


### Sep 3rd (Wednesday)

* [New MundaneMatt video dissecting the assertion gamers are "like nazis"](https://www.youtube.com/watch?v=TO589ti_-Pc)

* [Boogie releases a new video supporting GamerGate!](https://www.youtube.com/watch?v=wbQk5YqjO0E)

* Ben Quintero "downgraded" on gamasutra: http://loltaku.tumblr.com/post/96517102414/here-is-the-article-he-was-demoted-for-writing

* [The Guardian](http://en.wikipedia.org/wiki/The_Guardian) cuts off freelance writer Jenn after [her writing of an article biased against GamerGate](http://a.pomf.se/qnkglm.png): read [tweet 1](https://twitter.com/jennatar/status/507411245717135360), [tweet 2](https://twitter.com/jennatar/status/507411245717135360)

* Mod on #4chan IRC kicking people for complaining about the banning and autosaging: http://i.imgur.com/pBzNGlV.png

* [NOTE2: autosaging is back agan] [NOTE: not anymore it seems] /v/ was autosaging #gamergate threads and banning OPs: http://i.imgur.com/yGNIMpI.png


### Sep 2nd (Tuesday)

* Lawfag and IA might have some glorious plans for the future: http://a.pomf.se/ohuszo.png

* [NOTE: he's back now with a new trip] lawfag banned from /v/: http://ask.fm/Lawfag/answer/119126950824

* Based boogie2988 starts a petition on change.org against the agenda versus gamers: http://www.change.org/p/the-gaming-industry-please-stop-the-hate

* The BBC publishes a one-sided and biased pro-Sarkeesian article: https://archive.today/RpO7w

* Totalbiscuit's opinion on pretentious indie devs (warning: singing): https://soundcloud.com/totalbiscuit/shania-bain-that-dont-impressa-me-much

* The Guardian edits their article with a disclaimer stating the author "purchased and supported [Sarkeesian's] work": http://i.imgur.com/tsCj6fw.png

* @legobutts selling t-shirts based on this drama (she's directly involved in) and might be profiting of it: http://i.imgur.com/lDdFWhI.png

* SR4 creative director voices his support for Anita Sarkeesian: https://archive.today/cebK5

* IGN joined the Gamer+ side (no surprise here): https://archive.today/RsfWv


### Sep 1st (Monday)

* breitbart.com article, pretty heavily biased but still in support of us - http://www.breitbart.com/Breitbart-London/2014/09/01/Lying-Greedy-Promiscuous-Feminist-Bullies-are-Tearing-the-Video-Game-Industry-Apart

* op disrespectul nod in full effect, keep it up - http://i.imgur.com/sdL2EAN.jpg

* they have shills at the fucking guardian now - https://archive.today/eHC9z

* Gaming site promising to have actual journalistic standards is announced: http://www.goodgamers.us/

* Based female gamer Jemry Mellows giving her opinion on this whole subject: https://www.youtube.com/watch?v=tyscI9wZ8Bk

* Pastebin of a deleted google doc detailing how Silverstring Media is trying to push a "feminist" agenda in the gaming industry - http://pastebin.com/hnxggVBw

* Daniel Vavra, designer of the Mafia series, is on our side: http://a.pomf.se/rxtbws.png

* Gamasutra, Polygon, Kotaku and RPS losing traffic (possibly due to #GamerGate?): http://i.imgur.com/1wyEap9.png

* How to win IndieArcade: http://i.imgur.com/x3HO47j.jpg


## August 2014


### Aug 31st (Sunday)

* [LeoPirate on #GamerGate and corruption](http://youtu.be/9rTFDhVmnUE)

* vox media possibly coming under pressure from samsung due to this mess? - http://i.imgur.com/zNpjpxp.jpg

* people being escorted off PAX for mentioning #gamergate? - http://i.imgur.com/p42lPz3.png

* boogie2988 is on /v/ and supports us - http://a.pomf.se/nedoaq.png

* Yellow journalism Mk 2 (Allegations of 4chan hacking and doxxing Zoe Quinn) - https://archive.today/3QoY3


### Aug 30th (Saturday)

* The Escapist takes a stand for integrity - http://www.escapistmagazine.com/forums/read/18.858347-Zoe-Quinn-and-the-surrounding-controversy?page=375#21330098

* Maya Felix Kramer (aka @legobutts) Call Out - http://pastebin.com/qWX5qTvB

* Leigh Alexander calling Adam Baldwin a crackhead - http://i.imgur.com/Dw4oAnq.png

* Leigh Alexander getting BTFO'd by MundaneMatt - https://www.youtube.com/watch?v=XJf6sFThGbs

* Christina H. Sommers (an actual feminist) supports our cause - http://i.imgur.com/pavWden.png

* Based Jontron rant - http://youtu.be/cf7i0z2Zi7U?t=1h17m2s

* Possible happening on monday at PAX - http://i.imgur.com/0TMWn75.png http://i.imgur.com/Bq8Dywe.jpg

* New IA video incoming - https://twitter.com/Int_Aristocrat/status/505667357990060032

* Techraptor shadowbanned from reddit for reporting on this - http://i.imgur.com/I9ssnGJ.png


### Aug 28th (Thursday)

* "Gamers Are Dead" articles appear across multiple online games websites. Evidence for possible collusion? - http://markdownshare.com/view/a524affd-e679-40be-8aa1-72058065dc2a http://i.imgur.com/ZJ0nDpt.jpg

* this "gamer+" shit has been in planning for over a year now - https://groups.google.com/forum/#!topic/game-words-incorporated/VxYXZhDqv8I


### Aug 27th (Wednesday)

* @MissAngerist posts a twitlonger on her reassesement of events to date: https://twitter.com/MissAngerist/status/504234850560536576

* Adam Baldwin coins the hashtag "#Gamergate" in a tweet: https://twitter.com/AdamBaldwin/status/504801169638567936


### Aug 25th (Monday)

* Zoe Quinn on TFYC - http://youtu.be/xiWADnV8pmw

### Aug 24th (Sunday)

* What Really happened(MundaneMatt Guest) - https://www.youtube.com/watch?v=FV_8RHSyS14

* FIRE RISES - Losing ROPE
http://i.imgur.com/qMMVHER.jpg

* ISIS joke by BETA SJW
http://i.imgur.com/sNCIuBR.png

* TFYC IndieGoGo account is hacked
Screenshot hacked IndieGoGo - http://i.imgur.com/EKNGSDJ.jpg
(btw no fiddles and fries, if the project doesn't get funded we get our money back)

* Censorship of #Gamergate on Reddit - http://imgur.com/a/f4WDf

* Wolf Wozniak PAX Passes - http://i.imgur.com/cr6tfHr.jpg

* Ex-boyfriend speaks - http://www.reddit.com/user/qrios
Ex-boyfriend speaks part deux -http://antinegationism.tumblr.com/post/95602910146/what-all-that-zoe-quinn-stuff-was-about-a-totally

* Kotaku breaking their own rule on ETHICS
http://i.imgur.com/hE1US8k.png

* More Conflict of Interest/Nepotism - http://imgur.com/a/bqhRY
http://imgur.com/a/x0NpT
http://i.imgur.com/Wg6iLvL.jpg

### Aug 22nd (Friday)

* TFYC publishes a 4chan requested video on great female developers. The [video charts the career and influence of Roberta William](https://www.youtube.com/watch?v=rn7Q4XbWqvQ&list=UUhwoDCOjliin3x0Y_ShiGGw)

* Together with 4chan users, TFYC create the 4chan themed female gamer character, [Vivian James](http://knowyourmeme.com/memes/vivian-james), who is to appear the TFYC game they are now helping to fund. Vivian James later becomes one of the major memes/icons of the #Gamergate scandal.

* Honey Badger Radio: Discussing Zoe Quinn and Feminist Mean Girls : Karen Straughan talks - https://www.youtube.com/watch?v=cuFSMi5G3R4

* Youtube InternetAristocrat releases [a second video on the controversy](http://www.youtube.com/watch?v=pKmy5OKg6lo), discussing the  among games journalists and websites, contrasted with the intense interest among gamers. Contrasts the journalists omertà with their earlier frenzies over Max Temkin, and David Jaffe. Discusses journalists Patreon dontations to Quinn, and the GAME_JAM controvery.

* Phil Fish's company Polytron is hacked. Phil Fish annouces this, on twitter, and also announces that he is leaving video game development and putting the company and Fez 2 up for sale. [Image of tweets](http://imgur.com/uDIaZkE) [Reddit Thread](http://www.reddit.com/r/Games/comments/2e9cyn/phil_fish_deletes_twitter_account_after_polytron/). His twitter is subsequently locked.

### Aug 21st (Thursday)

* A Youtube interview with TFYC is published, describing the story of their project and its difficulties with Zoe Quinn  - http://youtu.be/1d6Q3VpqXyk

* 4chan users [propose a donation campaign](http://i3.kym-cdn.com/photos/images/original/000/816/363/e9f.png) from 4chan users to the TFYC Indiegogo campaign.  [\[AT\]](https://archive.today/5eVCS)

### Aug 20th (Wednesday)

* The Editor of Kotaku, Stephen Totilo [issues a statement](https://archive.today/8KjOG), saying that neither he nor the leadership team has found compelling evidence that Nathan Grayson was trading sex for positive review coverage in the case of Zoe Quinn. Totilo had earlier made a series of tweets on this event.

* It is revealed that Quinn's game Depression Quest recieved an IndieCade Award, awarded by a panel which included Robin Arnott, one of the people Eron alleges Quinn had an affair with. [\[TW\]](https://twitter.com/FartToContinue/status/502132193771421696/photo/1)

* NeoGAF forum owner Tyler Malka [creates a thread mocking the emails](http://www.neogaf.com/forum/showthread.php?t=878576) he has recieved from users complaining about the censorship of the controversy on the NeoGAF forums. [\[AT\]](https://archive.today/tgRkl)

### Aug 19th (Tuesday)

* Youtuber video game critic "Total Biscuit" makes [a twitlonger post](http://www.twitlonger.com/show/n_1s4nmr1/) discussing the Quinn scandal, decrying the misuse of the DMCA act to censor criticsm, also noting the current "massive nepotism problem" in games journalism, and the already intense nature of debate around the scandal.  [\[TW\]](https://twitter.com/Totalbiscuit/status/501616744411443201) [\[AT\]](https://archive.today/http://www.twitlonger.com/show/n_1s4nmr1/)

* TotalBiscuit's post is reposted in the Reddit [/r/gaming](http://www.reddit.com/r/gaming) subreddit. Reddit moderators delete 25,000 comments [in the thread](http://www.reddit.com/r/gaming/comments/2dz0gs/totalbiscuit_discusses_the_state_of_games/). [\[AT\]](https://archive.today/ENcGC).
* One of the /r/Gaming moderators was in [contact with Quinn](http://i.imgur.com/0ZAaahT.png). The same moderator [states](http://www.reddit.com/r/gaming/comments/2dzrlv/on_zoe_quinn_censorship_doxxing_and_general/) that the mass deletions were to prevent doxxing
* Gamers unable to speak in /r/gaming flood into related subreddits such as [/r/games, which are also been deleting comments](http://www.reddit.com/r/Games/comments/2dzpmx/rgames_meta_discussion_500000_readers_zoe_quinn/). /r/Games has a readership of 500,000 at this point. [\[AT\]](https://archive.today/Ru0PK)

* Most major gaming websites such as NeoGAF, and the [Gamespot forums](http://www.gamespot.com/forums/system-wars-314159282/zoe-quinn-saga-reddit-4chan-ect-being-censored-by--31506935/) heavily moderate, edit, delete, and censor discussion of the controversy. 4chan /v/ mods are also allegedly also deleting threads. [The Escapist Forums](http://www.escapistmagazine.com/forums/read/18.858347-Zoe-Quinn-and-the-surrounding-controversy) remains one of the few major websites still permitting open discussion of the controversy. 

* It is later revealed in one of the [GameJournoPro Email List Leaks](http://yiannopoulos.net/2014/09/19/gamejournopros-zoe-quinn-email-dump/) that the Escapist's Editor Greg Tito was put under considerable pressure to shut down the Escapist forum discussion by other members of the secret mailing list, including Ben Kuchera of Polygon, and Kyle Orland of Ars Technica.

* Steam users are banned from the depression quest forums after critiquing depression quest. The bans are given for "abuse". http://i.imgur.com/VwHDs2Z.jpg

### Aug 18th (Monday)

* Youtuber InternetAristocrat releases a video discussing the ongoing fallout from Eron's post, and its implications for the state of games journalism. [Link to Video on Youtube](https://www.youtube.com/watch?v=C5-51PfwI3M). The video also discusses the suppression of the TFYC scandal on game news sites.

* The #burgersandfries public irc chatroom is created for discussion of the rapidly emerging events.

### Aug 17th (Sunday)

* Youtuber MundaneMatt [releases a video discussing Eron's post](https://www.youtube.com/watch?v=O5CXOafuTXM), highlighting the hypocricy in games journalism, and questioning the ethics and integrity of some in the games industry.

* Zoe Quinn issues a [Digital Millenium Copyright Act](http://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act) takedown notice on Mundane Matt's video, alleging copyright infringement ([DMCA notice](http://knowyourmeme.com/photos/815645-quinnspiracy)). The video is taken down by Google. (The DMCA order would later be released on ~[Aug 21st](https://twitter.com/mundanematt/status/502575957178580993))

* Threads appear in The Escapist Magazine forums discussing the controversy [Thread](http://www.escapistmagazine.com/forums/read/18.858347-Zoe-Quinn-and-the-surrounding-controversy). [\[AT\]](https://archive.today/fhR59). Posting is subject to very heavy moderation.

* TFYC (The Fine Young Capitalists) [alleged doxxing and harrassment](http://imgur.com/V3nHOOI) which their project received from Zoe Quinn, in a [Reddit thread](http://www.reddit.com/r/TumblrInAction/comments/2dt5hu/depression_quest_dev_claims_harassment_and/) disscussing Quinn's earlier harrassment allegations against WC.

### Aug 16th (Saturday)

* Eron Gjoni publishes the Zoe Post: http://thezoepost.wordpress.com/ ; https://archive.today/YWrmI. 

 The long post outlines Eron's breakup with a game developer called Zoe Quinn. The post reveals that Quinn had had affairs with several prominent individuals in the games industry.
 In particular, it is revealed that Quinn had an affair with games journalist Nathan Grayson, who is noted to write for Kotaku, one of the largest game news websites in the world.
 For several years, Kotaku and other major gaming news sites, had adopted editorial stances highly critical of gaming and gamers' "sexist" attitudes towards women.

* Threads discussing Eron's post first appear on the somethingawful.com and Penny Arcade forums. The threads are deleted.

* A fellow somethingawful.com forum poster informs Quinn of the existence of Eron's post via [twitter](https://twitter.com/TheQuinnspiracy/status/500489989109465088). [\[AT\]](https://archive.today/CizL0)  

* Approximately 6 hours after its creation, the first imageboard threads discussing Eron's post appear on 4chan. Archived Versions: [1](https://archive.moe/v/thread/258174703/), [2](https://archive.moe/v/thread/258179638/), [3](https://archive.moe/v/thread/258182378/). There is little consensus among the anonymous posters. These threads are also deleted.

### Aug 12th (Tuesday)

* TFYC IndieGoGo Campaign Started - https://www.indiegogo.com/projects/the-fine-young-capitalists--2/x/8485163

------------------------------

# Pre #Gamergate Scandals, Events, and information

## 2014

### March 2014

* [GAME_JAM Controversy](https://archive.today/MNLyp)

### Jan 8th

* Nathan Grayson gives Zoe Quinn's game Depression Quest a prominant viewing in [his Rock Paper Shotgun article on recent Value Greenlights](http://www.rockpapershotgun.com/2014/01/08/admission-quest-valve-greenlights-50-more-games/) [\[AT\]](https://archive.today/38dwH)

## 2013

* A Larian studios (divinity: original sin) employee speaks out against SJWs affecting gaming: http://orogion.deviantart.com/journal/Save-the-Boob-plate-380891149

* Cliffy B has a brother who works for the conglomerate Polygon belongs to - http://i.imgur.com/DAbE8nY.jpg

